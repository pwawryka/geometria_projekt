# -*- coding: utf-8 -*-

import numpy as np
from PyQt4.QtGui import QMainWindow, QMessageBox, QFileDialog, QGraphicsScene, QPen, QColor, QBrush

from algorithm import Solver
from ui.main_window import Ui_MainWindow


def show_error(msg):
    box = QMessageBox()
    box.setWindowTitle('Błąd')
    box.setText(msg)
    box.exec_()


class MainWindow(QMainWindow, Ui_MainWindow):
    NONE = 0
    DRAWING = 1
    POLYGON_DRAWING = 2
    START_POINT_DRAWING = 3
    END_POINT_DRAWING = 4
    REMOVE_POLYGON = 5
    RESULT = 6

    PRESSED_BUTTON_SS = 'background: qradialgradient(spread:repeat, cx:0.5, cy:0.5, radius:0.077, fx:0.5, fy:0.5, ' \
                        'stop:0 rgba(0, 0, 0, 36), stop:0.505682 rgba(0, 0, 0, 34), stop:1 rgba(0, 0, 0, 0))'

    def __init__(self):
        QMainWindow.__init__(self)
        self.setupUi(self)

        self.startEditionButton.clicked.connect(self.start_edition_button)
        self.endEditionButton.clicked.connect(self.end_edition_button)
        self.endEditionButton.setEnabled(False)
        self.polygonButton.clicked.connect(self.polygon_button)
        self.polygonButton.setEnabled(False)
        self.startPointButton.clicked.connect(self.start_point_button)
        self.startPointButton.setEnabled(False)
        self.endPointButton.clicked.connect(self.end_point_button)
        self.endPointButton.setEnabled(False)
        self.removePolygonButton.clicked.connect(self.remove_polygon_button)
        self.removePolygonButton.setEnabled(False)
        self.clearMap.clicked.connect(self.clear_map)
        self.clearMap.setEnabled(False)
        self.loadMapButton.clicked.connect(self.load_map_button)
        self.loadMapButton.setEnabled(False)
        self.saveMapButton.clicked.connect(self.save_map_button)
        self.saveMapButton.setEnabled(False)

        self.runButton.clicked.connect(self.run_button)
        self.endButton.clicked.connect(self.end_button)

        self.scene = self.create_new_scene()
        self.graphicsView.mousePressEvent = self.mouse_press_event

        self.state = self.NONE
        self.drawing_state = self.NONE

        self.eps = 10
        self.figures = []
        self.current_figure = []
        self.start_point = None
        self.end_point = None

    def end_button(self, *args, **kwargs):
        self.state = self.NONE
        self.startEditionButton.setEnabled(True)
        self.runButton.setEnabled(True)
        self.radiusInput.setEnabled(True)
        self.resize_window(width=760)
        self.redraw_scene()

    def draw_result(self, result):
        for index, point in enumerate(result[0][1:-1]):
            self.draw_point(point, int(self.radiusInput.text()))
            self.draw_line(result[0][index], result[0][index+1])
        self.draw_line(result[0][-2], result[0][-1])

    def resize_window(self, width=None, height=None):
        if not width:
            width = self.width()
        if not height:
            height = self.height()

        self.setMinimumWidth(width)
        self.setMaximumWidth(width)
        self.setMinimumHeight(height)
        self.setMaximumHeight(height)
        self.setGeometry(self.geometry().x(), self.geometry().y(), width, height)

    def run_button(self, *args, **kwargs):
        try:
            self.startEditionButton.setEnabled(False)
            self.runButton.setEnabled(False)
            self.radiusInput.setEnabled(False)
            radius = float(self.radiusInput.text())
            corner = [self.scene.width(), self.scene.height()]
            result = Solver(self.start_point, self.end_point, self.figures, radius, corner).run()
            self.draw_result(result)
            self.pathLabel.setText(str(result[1]))
            self.resize_window(width=850)
            self.state = self.RESULT

        except IndexError:
            show_error('Nie znaleziono ścieżki')
            self.startEditionButton.setEnabled(True)
            self.runButton.setEnabled(True)
            self.radiusInput.setEnabled(True)
            self.resize_window(width=760)
        except ValueError:
            show_error('Wartość promienia musi być liczbą rzeczywistą')
            self.startEditionButton.setEnabled(True)
            self.runButton.setEnabled(True)
            self.radiusInput.setEnabled(True)
            self.resize_window(width=760)

    def save_map(self, file):
        file.write('{},{}\n'.format(self.start_point[0], self.start_point[1]))
        file.write('{},{}\n'.format(self.end_point[0], self.end_point[1]))
        for figure in self.figures:
            for point in figure:
                file.write('{},{};'.format(point[0], point[1]))
            file.write('\n')

    def load_map(self, file):
        self.start_point = None
        self.end_point = None
        self.figures = []
        self.current_figure = []

        for index, line in enumerate(file.readlines()):
            if index == 0:
                try:
                    data = line[:-1].split(',')
                    self.start_point = [float(data[0]), float(data[1])]
                except ValueError as e:
                    show_error(str(e))
            elif index == 1:
                try:
                    data = line[:-1].split(',')
                    self.end_point = [float(data[0]), float(data[1])]
                except ValueError as e:
                    show_error(str(e))
            else:
                try:
                    figure = []
                    points = line[:-1].split(';')
                    for point in points[:-1]:
                        data = point.split(',')
                        figure.append([float(data[0]), float(data[1])])
                    self.figures.append(figure)
                except ValueError as e:
                    show_error(str(e))

        self.redraw_scene()

    def save_map_button(self, *args, **kwargs):
        filename = QFileDialog.getSaveFileName(self, 'Zapisz plik', '')
        with open(filename, 'w+') as file:
            self.save_map(file)

    def load_map_button(self, *args, **kwargs):
        try:
            filename = QFileDialog.getOpenFileName(self, 'Otwórz plik', '')
            with open(filename, 'r') as file:
                self.load_map(file)
        except FileNotFoundError:
            pass

    def end_edition_button(self, *args, **kwargs):
        self.state = self.NONE
        self.drawing_state = self.NONE
        self.startPointButton.setStyleSheet('')
        self.endPointButton.setStyleSheet('')
        self.polygonButton.setStyleSheet('')
        self.removePolygonButton.setStyleSheet('')
        self.polygonButton.setEnabled(False)
        self.startPointButton.setEnabled(False)
        self.endPointButton.setEnabled(False)
        self.endEditionButton.setEnabled(False)
        self.removePolygonButton.setEnabled(False)
        self.loadMapButton.setEnabled(False)
        self.saveMapButton.setEnabled(False)
        self.clearMap.setEnabled(False)
        self.startEditionButton.setEnabled(True)
        self.runButton.setEnabled(True)
        self.radiusInput.setEnabled(True)

    def start_edition_button(self, *args, **kwargs):
        self.state = self.DRAWING
        self.polygonButton.setEnabled(True)
        self.startPointButton.setEnabled(True)
        self.endPointButton.setEnabled(True)
        self.removePolygonButton.setEnabled(True)
        self.endEditionButton.setEnabled(True)
        self.removePolygonButton.setEnabled(True)
        self.loadMapButton.setEnabled(True)
        self.saveMapButton.setEnabled(True)
        self.clearMap.setEnabled(True)
        self.startEditionButton.setEnabled(False)
        self.runButton.setEnabled(False)
        self.radiusInput.setEnabled(False)

    def clear_map(self):
        self.start_point = None
        self.end_point = None
        self.figures = []
        self.current_figure = []
        self.redraw_scene()

    def _push_drawing_button(self, button, drawing_state):
        self.drawing_state = drawing_state
        button.setStyleSheet(self.PRESSED_BUTTON_SS)
        for other_button in [self.polygonButton, self.startPointButton, self.endPointButton, self.removePolygonButton]:
            if button is not other_button:
                other_button.setStyleSheet('')

    def polygon_button(self, *args, **kwargs):
        self._push_drawing_button(self.polygonButton, self.POLYGON_DRAWING)

    def start_point_button(self, *args, **kwargs):
        self._push_drawing_button(self.startPointButton, self.START_POINT_DRAWING)

    def end_point_button(self, *args, **kwargs):
        self._push_drawing_button(self.endPointButton, self.END_POINT_DRAWING)

    def remove_polygon_button(self, *args, **kwargs):
        self._push_drawing_button(self.removePolygonButton, self.REMOVE_POLYGON)

    @staticmethod
    def distance(a, b):
        return np.sqrt(sum([(a[i] - b[i]) ** 2 for i in range(len(a))]))

    def draw_line(self, first, second):
        self.scene.addLine(first[0], first[1], second[0], second[1], QPen(QColor(0, 0, 0)))

    def create_new_scene(self):
        scene = QGraphicsScene(self)
        scene.setSceneRect(0, 0, self.graphicsView.width() - 5, self.graphicsView.height() - 5)
        self.graphicsView.setScene(scene)
        return scene

    def redraw_scene(self):
        self.scene = self.create_new_scene()

        if self.start_point is not None:
            self.draw_point(self.start_point, radius=20, color=QColor(255, 0, 0))

        if self.end_point is not None:
            self.draw_point(self.end_point, radius=20, color=QColor(0, 0, 255))

        for figure in self.figures:
            for index in range(len(figure)):
                self.draw_line(figure[index], figure[(index + 1) % len(figure)])

        if self.current_figure:
            self.draw_point(self.current_figure[-1])
            for index in range(len(self.current_figure) - 1):
                self.draw_point(self.current_figure[index])
                self.draw_line(self.current_figure[index], self.current_figure[index + 1])

    def draw_point(self, point, radius=1, color=QColor(0, 0, 0)):
        scene = self.graphicsView.scene()
        scene.addEllipse(point[0] - radius, point[1] - radius, 2 * radius, 2 * radius, QPen(color), QBrush())

    def det(self, pt1, pt2, pt3):
        det = (pt1[0] - pt3[0]) * (pt2[1] - pt3[1]) - (pt1[1] - pt3[1]) * (pt2[0] - pt3[0])
        if det < 0:
            return -1
        elif det > 0:
            return 1
        return 0

    def _remove_polygon(self, point):
        for index, figure in enumerate(self.figures):
            _sum = sum([self.det(figure[i], figure[(i + 1) % len(figure)], point) for i in range(len(figure))])
            if np.abs(_sum) == len(figure):
                del self.figures[index]
                break

    def mouse_press_event(self, event):
        if self.state == self.DRAWING:
            if self.drawing_state == self.POLYGON_DRAWING:
                new_point = [event.pos().x(), event.pos().y()]
                if self.current_figure and self.distance(self.current_figure[0], new_point) < self.eps:
                    self.figures.append(self.current_figure)
                    self.current_figure = []
                else:
                    self.current_figure.append(new_point)
            elif self.drawing_state == self.START_POINT_DRAWING:
                self.start_point = [event.pos().x(), event.pos().y()]
            elif self.drawing_state == self.END_POINT_DRAWING:
                self.end_point = [event.pos().x(), event.pos().y()]
            elif self.drawing_state == self.REMOVE_POLYGON:
                point = [event.pos().x(), event.pos().y()]
                self._remove_polygon(point)

            self.redraw_scene()

# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui/main_window.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName(_fromUtf8("MainWindow"))
        MainWindow.resize(760, 485)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Ignored, QtGui.QSizePolicy.Ignored)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(MainWindow.sizePolicy().hasHeightForWidth())
        MainWindow.setSizePolicy(sizePolicy)
        MainWindow.setMinimumSize(QtCore.QSize(760, 485))
        MainWindow.setMaximumSize(QtCore.QSize(760, 485))
        self.centralwidget = QtGui.QWidget(MainWindow)
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))
        self.graphicsView = QtGui.QGraphicsView(self.centralwidget)
        self.graphicsView.setGeometry(QtCore.QRect(210, 10, 541, 431))
        self.graphicsView.setObjectName(_fromUtf8("graphicsView"))
        self.groupBox = QtGui.QGroupBox(self.centralwidget)
        self.groupBox.setGeometry(QtCore.QRect(10, 110, 191, 231))
        self.groupBox.setObjectName(_fromUtf8("groupBox"))
        self.startPointButton = QtGui.QPushButton(self.groupBox)
        self.startPointButton.setGeometry(QtCore.QRect(100, 80, 75, 41))
        self.startPointButton.setObjectName(_fromUtf8("startPointButton"))
        self.endEditionButton = QtGui.QPushButton(self.groupBox)
        self.endEditionButton.setGeometry(QtCore.QRect(10, 30, 75, 41))
        self.endEditionButton.setObjectName(_fromUtf8("endEditionButton"))
        self.endPointButton = QtGui.QPushButton(self.groupBox)
        self.endPointButton.setGeometry(QtCore.QRect(100, 130, 75, 41))
        self.endPointButton.setObjectName(_fromUtf8("endPointButton"))
        self.polygonButton = QtGui.QPushButton(self.groupBox)
        self.polygonButton.setGeometry(QtCore.QRect(100, 30, 75, 41))
        self.polygonButton.setAutoFillBackground(False)
        self.polygonButton.setStyleSheet(_fromUtf8(""))
        self.polygonButton.setObjectName(_fromUtf8("polygonButton"))
        self.saveMapButton = QtGui.QPushButton(self.groupBox)
        self.saveMapButton.setGeometry(QtCore.QRect(10, 80, 75, 41))
        self.saveMapButton.setObjectName(_fromUtf8("saveMapButton"))
        self.loadMapButton = QtGui.QPushButton(self.groupBox)
        self.loadMapButton.setGeometry(QtCore.QRect(10, 130, 75, 41))
        self.loadMapButton.setObjectName(_fromUtf8("loadMapButton"))
        self.removePolygonButton = QtGui.QPushButton(self.groupBox)
        self.removePolygonButton.setGeometry(QtCore.QRect(100, 180, 75, 41))
        self.removePolygonButton.setObjectName(_fromUtf8("removePolygonButton"))
        self.clearMap = QtGui.QPushButton(self.groupBox)
        self.clearMap.setGeometry(QtCore.QRect(10, 180, 75, 41))
        self.clearMap.setObjectName(_fromUtf8("clearMap"))
        self.groupBox_2 = QtGui.QGroupBox(self.centralwidget)
        self.groupBox_2.setGeometry(QtCore.QRect(10, 350, 191, 91))
        self.groupBox_2.setTitle(_fromUtf8(""))
        self.groupBox_2.setObjectName(_fromUtf8("groupBox_2"))
        self.runButton = QtGui.QPushButton(self.groupBox_2)
        self.runButton.setGeometry(QtCore.QRect(100, 40, 75, 41))
        self.runButton.setObjectName(_fromUtf8("runButton"))
        self.startEditionButton = QtGui.QPushButton(self.groupBox_2)
        self.startEditionButton.setGeometry(QtCore.QRect(10, 40, 75, 41))
        self.startEditionButton.setObjectName(_fromUtf8("startEditionButton"))
        self.label = QtGui.QLabel(self.groupBox_2)
        self.label.setGeometry(QtCore.QRect(100, 10, 81, 21))
        self.label.setObjectName(_fromUtf8("label"))
        self.radiusInput = QtGui.QLineEdit(self.groupBox_2)
        self.radiusInput.setGeometry(QtCore.QRect(10, 10, 81, 20))
        self.radiusInput.setObjectName(_fromUtf8("radiusInput"))
        self.runButton.raise_()
        self.startEditionButton.raise_()
        self.label.raise_()
        self.radiusInput.raise_()
        self.groupBox_3 = QtGui.QGroupBox(self.centralwidget)
        self.groupBox_3.setGeometry(QtCore.QRect(10, 10, 181, 91))
        self.groupBox_3.setObjectName(_fromUtf8("groupBox_3"))
        self.label_2 = QtGui.QLabel(self.groupBox_3)
        self.label_2.setGeometry(QtCore.QRect(10, 20, 91, 16))
        self.label_2.setStyleSheet(_fromUtf8("color: rgb(255, 0, 0)"))
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.label_3 = QtGui.QLabel(self.groupBox_3)
        self.label_3.setGeometry(QtCore.QRect(10, 40, 81, 16))
        self.label_3.setStyleSheet(_fromUtf8("color: rgba(0, 0, 255, 255)"))
        self.label_3.setObjectName(_fromUtf8("label_3"))
        self.label_4 = QtGui.QLabel(self.groupBox_3)
        self.label_4.setGeometry(QtCore.QRect(10, 60, 47, 13))
        self.label_4.setObjectName(_fromUtf8("label_4"))
        self.endButton = QtGui.QPushButton(self.centralwidget)
        self.endButton.setGeometry(QtCore.QRect(760, 100, 81, 41))
        self.endButton.setObjectName(_fromUtf8("endButton"))
        self.label_5 = QtGui.QLabel(self.centralwidget)
        self.label_5.setGeometry(QtCore.QRect(760, 10, 81, 41))
        self.label_5.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.label_5.setStyleSheet(_fromUtf8(""))
        self.label_5.setObjectName(_fromUtf8("label_5"))
        self.pathLabel = QtGui.QLabel(self.centralwidget)
        self.pathLabel.setGeometry(QtCore.QRect(760, 40, 81, 41))
        self.pathLabel.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.pathLabel.setStyleSheet(_fromUtf8(""))
        self.pathLabel.setObjectName(_fromUtf8("pathLabel"))
        MainWindow.setCentralWidget(self.centralwidget)
        self.statusbar = QtGui.QStatusBar(MainWindow)
        self.statusbar.setObjectName(_fromUtf8("statusbar"))
        MainWindow.setStatusBar(self.statusbar)
        self.menubar = QtGui.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 760, 21))
        self.menubar.setObjectName(_fromUtf8("menubar"))
        MainWindow.setMenuBar(self.menubar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(_translate("MainWindow", "Robot", None))
        self.groupBox.setTitle(_translate("MainWindow", "Edytowanie", None))
        self.startPointButton.setText(_translate("MainWindow", "Punkt \n"
"początkowy", None))
        self.endEditionButton.setText(_translate("MainWindow", "Zakończ\n"
" edycję", None))
        self.endPointButton.setText(_translate("MainWindow", "Punkt \n"
"końcowy", None))
        self.polygonButton.setText(_translate("MainWindow", "Wielokąt", None))
        self.saveMapButton.setText(_translate("MainWindow", "Zapisz", None))
        self.loadMapButton.setText(_translate("MainWindow", "Wczytaj", None))
        self.removePolygonButton.setText(_translate("MainWindow", "Usuń\n"
" wielokąt", None))
        self.clearMap.setText(_translate("MainWindow", "Wyczyść\n"
" mapę", None))
        self.runButton.setText(_translate("MainWindow", "Uruchom\n"
" algorytm", None))
        self.startEditionButton.setText(_translate("MainWindow", "Edytuj mapę", None))
        self.label.setText(_translate("MainWindow", "promień robota", None))
        self.radiusInput.setText(_translate("MainWindow", "20", None))
        self.groupBox_3.setTitle(_translate("MainWindow", "Legenda", None))
        self.label_2.setText(_translate("MainWindow", "Punkt początkowy", None))
        self.label_3.setText(_translate("MainWindow", "Punkt końcowy", None))
        self.label_4.setText(_translate("MainWindow", "Wielokąt", None))
        self.endButton.setText(_translate("MainWindow", "Zakończ", None))
        self.label_5.setText(_translate("MainWindow", "Długość ścieżki\n"
" wynosi:", None))
        self.pathLabel.setText(_translate("MainWindow", "0", None))


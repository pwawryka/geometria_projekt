import sys
from PyQt4.QtGui import QApplication
from ui.controller import MainWindow


def main():
    app = QApplication(sys.argv)
    window = MainWindow()
    window.show()

    sys.exit(app.exec_())


if __name__ == '__main__':
    main()

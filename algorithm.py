import math
import numpy


class Solver(object):

    def __init__(self, start, finish, figures, radius, corner):
        self.angles = []
        self.start = start
        self.finish = finish
        self.figures = figures
        self.corner = corner
        self.resized_figures = self.resize_figures(figures, radius)
        self.lines = self.get_lines(self.resized_figures)
        self.points = self.get_points(self.resized_figures, self.finish)
        self.points.insert(0, start)
        self.set_bounds(self.lines, corner, radius)
        self.possible_paths = []
        self.graph = self.create_graph(self.points)
        self.min_len = numpy.inf

    def which_side(self, pt1, pt2, pt3):
        return (pt1[0] - pt3[0]) * (pt2[1] - pt3[1]) - (pt1[1] - pt3[1]) * (pt2[0] - pt3[0])

    def same_side(self, pt1, pt2, pt3, pt4):
        """determines if pt3 and pt4 lie on the same side of line (pt1,pt2)"""

        sides = self.which_side(pt1, pt2, pt3) * self.which_side(pt1, pt2, pt4)
        if sides > 0:
            return 1
        elif sides == 0:
            return 0
        return -1

    def get_length(self, line):
        return math.sqrt((line[0][0] - line[1][0]) ** 2 + (line[0][1] - line[1][1]) ** 2)

    def point_shift(self, angle, radius):
        pt1 = angle[0]
        pt2 = angle[1]
        pt3 = angle[2]

        vy1 = pt2[0] - pt1[0]
        vx1 = pt1[1] - pt2[1]
        len1 = self.get_length([[vx1, vy1], [0, 0]])
        vector1 = [(vx1*radius)/len1, (vy1*radius)/len1]

        vy2 = pt3[0] - pt2[0]
        vx2 = pt2[1] - pt3[1]
        len2 = self.get_length([[vx2, vy2], [0, 0]])
        vector2 = [(vx2 * radius) / len2, (vy2 * radius) / len2]

        if self.same_side(pt1, pt2, pt3, self.add_vector(pt1, vector1)) > 0:
            vector1[0] *= -1
            vector1[1] *= -1

        if self.same_side(pt2, pt3, pt1, self.add_vector(pt2, vector2)) > 0:
            vector2[0] *= -1
            vector2[1] *= -1

        res = [self.add_vector(pt2, vector1), self.add_vector(pt2, vector2)]


        line1 = [self.add_vector(pt1, vector1), self.add_vector(pt2, vector1)]
        line2 = [self.add_vector(pt2, vector2), self.add_vector(pt3, vector2)]

        if line1[0][0] == line1[1][0]:
            x = line1[0][0]
            y = line2[0][1] + ((line2[1][1] - line2[0][1]) * (x - line2[0][0])) / (line2[1][0] - line2[0][0])
        elif line2[0][0] == line2[1][0]:
            x = line2[0][0]
            y = line1[0][1] + ((line1[1][1] - line1[0][1]) * (x - line1[0][0])) / (line1[1][0] - line1[0][0])
        else:
            a1 = (line1[0][1] - line1[1][1]) / (line1[0][0] - line1[1][0])
            b1 = line1[0][1] + ((line1[1][1] - line1[0][1]) * (-1 * line1[0][0])) / (line1[1][0] - line1[0][0])
            a2 = (line2[0][1] - line2[1][1])/(line2[0][0] - line2[1][0])
            b2 = line2[0][1] + ((line2[1][1] - line2[0][1]) * (-1 * line2[0][0])) / (line2[1][0] - line2[0][0])
            x = (b2 - b1) / (a1 - a2)
            y = line2[0][1] + ((line2[1][1] - line2[0][1]) * (x - line2[0][0])) / (line2[1][0] - line2[0][0])

        vector3 = [x*radius / self.get_length([[x, y], [0, 0]]), y*radius / self.get_length([[x, y], [0, 0]])]
        res.append(self.add_vector(pt2, vector3))
        self.angles.append(res)
        return res

    def next_pt(self, pt, figure):
        return figure[(figure.index(pt) + 1) % len(figure)]

    def add_vector(self, v1, v2):
        return [v1[i] + v2[i] for i in range(len(v1))]

    def resize_figure(self, figure, radius):
        angles = []
        resized = figure[:]
        for point_index in range(len(figure)):
            angles.append([figure[point_index], figure[(point_index+1) % len(figure)], figure[(point_index+2) % len(figure)]])
        for angle in angles:
            resized[figure.index(angle[1])] = self.point_shift(angle, radius)[0]
            resized.insert(figure.index(angle[1])+1, self.point_shift(angle, radius)[1])
            resized.insert(figure.index(angle[1])+2, self.point_shift(angle, radius)[2])
        return resized

    def resize_figures(self, figures, radius):
        return [self.resize_figure(figure, radius) for figure in figures]

    def get_lines(self, figures):
        return [[pt, self.next_pt(pt, figure)] for figure in figures for pt in figure]

    def get_points(self, figures, stop):
        pts = [pt for figure in figures for pt in figure]
        pts.append(stop)
        return pts

    def cross(self, line1, line2):
        """ returns  1 if lines cross
            returns  0 if any end of any line belongs to the other line
            returns -1 if lines do not cross
        """
        if self.same_side(line1[0], line1[1], line2[0], line2[1]) < 0 \
                and self.same_side(line2[0], line2[1], line1[0], line1[1]) < 0:
            return 1

        if self.same_side(line1[0], line1[1], line2[0], line2[1]) == 0 \
                or self.same_side(line2[0], line2[1], line1[0], line1[1]) == 0:
            return 0

        return -1

    def set_bounds(self, lines, corner, radius):
        lines.append([[radius, radius], [radius, corner[1] - radius]])
        lines.append([[radius, corner[1] - radius], [corner[0] - radius, corner[1] - radius]])
        lines.append([[corner[0] - radius, corner[1] - radius], [corner[0] - radius, radius]])
        lines.append([[corner[0] - radius, radius], [radius, radius]])
        return lines

    def create_graph(self, points):
        visibl={}
        print len(points)
        for point1 in points:
            visibl[tuple(point1)] = {}
            for point2 in points:
                visibl[tuple(point1)][tuple(point2)] = self.visitable(point1, point2, self.lines)
        print visibl
        return visibl


    def visitable(self, pt_from, pt_to, lines):
        crosses = [self.cross([pt_from, pt_to], line) > 0 for line in lines]

        for a in self.angles:
            if pt_from in a and pt_to in a and abs(a.index(pt_from) - a.index(pt_to)) == 1:
                return True

        if any(crosses):
            return False

        if [pt_from, pt_to] in lines or [pt_to, pt_from] in lines:
            return True

        if any([pt_from in figure and pt_to in figure for figure in self.resized_figures]):
            return False

        return True

    def visit(self, path, total_len, points, lines):
        if path[-1] == self.finish:
            self.possible_paths.append([path, total_len])
            if total_len < self.min_len:
                self.min_len = total_len
        else:
            for pt in points:
                #if self.visitable(path[-1], pt, lines):
                if self.graph[tuple(path[-1])][tuple(pt)] and total_len < self.min_len:
                    new_path = path[:]
                    new_path.append(pt)
                    new_points = points[:]
                    new_points.remove(pt)
                    self.visit(new_path, total_len + self.get_length([path[-1], pt]), new_points, lines)

        return self.possible_paths

    def run(self):
        result = self.visit([self.start], 0, self.points, self.lines)
        best_path = result[0]
        for r in result:
            if r[1] < best_path[1]:
                best_path = r

        return best_path
